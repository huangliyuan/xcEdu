package com.xuecheng.api.course;

import com.xuecheng.framework.domain.course.ext.CategoryNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

@Api(value="课程分组管理",description = "课程分组管理")
public interface CategoryControllerApi {

    @ApiOperation("查询分类")
    public List<CategoryNode> findList();
}
