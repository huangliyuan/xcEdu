package com.xuecheng.api.cms;

import com.xuecheng.framework.model.response.QueryResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "cms页面模板管理接口",description = "cms页面模板管理接口")
public interface CmsTemplateControllerApi {

    @ApiOperation("查询页面模板列表")
    public QueryResponseResult findList();
}
