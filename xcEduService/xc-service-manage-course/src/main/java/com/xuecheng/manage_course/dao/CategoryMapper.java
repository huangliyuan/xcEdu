package com.xuecheng.manage_course.dao;

import com.xuecheng.framework.domain.course.ext.CategoryNode;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component(value = "CategoryMapper")
public interface CategoryMapper {

    //查询课程分类
    public List<CategoryNode> selectList();
}
