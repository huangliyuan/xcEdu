package com.xuecheng.test.fastdfs;

import org.csource.common.MyException;
import org.csource.fastdfs.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Administrator
 * @version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestFastDFS {

    //上传文件
    @Test
    public void testUpload(){
        try {
            //加载配置文件
            ClientGlobal.initByProperties("config/fastdfs-client.properties");
            //定义trackerClient，请求trackerService
            TrackerClient trackerClient = new TrackerClient();
            //创建连接
            TrackerServer connection = trackerClient.getConnection();
            //获取storage
            StorageServer storageClient = trackerClient.getStoreStorage(connection);
            //创建storageClient
            StorageClient1 storageClient1 = new StorageClient1(connection,storageClient);
            //本地文件的路径
            String path = "F:/xcEdu/xcEduUI/xc-ui-pc-static-portal/img/add.jpg";
            String fileId = storageClient1.upload_file1(path, "jpg", null);
            System.out.println(fileId);
            //group1/M00/00/00/wKgZhVw9xHaAAeklAAAo94oD_6o150.jpg
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }
    }


    //下载文件
    @Test
    public void testDownload(){
        try {
            //加载配置文件
            ClientGlobal.initByProperties("config/fastdfs-client.properties");
            //定义trackerClient，请求trackerService
            TrackerClient trackerClient = new TrackerClient();
            //创建连接
            TrackerServer connection = trackerClient.getConnection();
            //获取storage
            StorageServer storageClient = trackerClient.getStoreStorage(connection);
            //创建storageClient
            StorageClient1 storageClient1 = new StorageClient1(connection,storageClient);
            //group1/M00/00/00/wKgZhVw9xHaAAeklAAAo94oD_6o150.jpg
            //文件下载
            byte[] bytes = storageClient1.download_file1("group1/M00/00/00/wKgZhVw9g92AUTyqAAAo94oD_6o926.jpg");
            //使用输出流输出
            FileOutputStream fileOutputStream = new FileOutputStream(new File("d://download.jpg"));
            fileOutputStream.write(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }
    }



}
