<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
Hello ${name}!
<hr>
<h4>list指令的案例</h4>
<TABLE>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#list stus as stu>
        <tr>
            <td>${stu_index + 1}</td>
            <td>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.mondy}</td>
        </tr>
    </#list>
</TABLE>
<hr>
<h4>map集合数据的遍历</h4>
<h5>遍历map集合有两种方法：[]内写key，或者map集合名称后面加.key</h5>
第一种方法遍历map集合<br>
姓名：${stuMap['stu1'].name}<br>
年龄：${stuMap['stu1'].age}<br>
第二种方法遍历map集合<br>
姓名：${stuMap.stu1.name}<br>
年龄：${stuMap.stu1.age}<br>
<hr>
<h4>用遍历的方式遍历map集合</h4>
<h6>使用if指令，如果姓名为“小明”显示背景色，如果钱金额大于300显示背景色</h6>
<h6>给遍历的内容加上空值判断，确保遍历的内容是存在的</h6>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#if stuMap??>
        <#list stuMap?keys as k>
            <tr>
                <td>${k_index + 1}</td>
                <td <#if stuMap[k].name == '小明'>style="background-color: red" </#if>>${(stuMap[k].name) ! ''}</td>
                <td>${stuMap[k].age}</td>
                <td <#if (stuMap[k].mondy > 300)>style="background-color: red" </#if>>${(stuMap[k].mondy) ! ''}</td>
            </tr>
        </#list>
    </#if>
</table>
<hr>
<h4>使用freeMarker内建函数</h4>
获取集合的大小：${stus?size}<br>
显示年月日:${stuMap.stu1.birthday?date}<br>
显示时分秒:${stuMap.stu1.birthday?time}<br>
显示日期+时间:${stuMap.stu1.birthday?datetime}<br>
显示自定义日期格式:${stuMap.stu1.birthday?string("yyyy年MM月dd日")}<br>
展示数据为数字格式的情况：${point}<br>
将展示的数字格式数据转为字符串展示：${point?c}<br>
</body>
</html>