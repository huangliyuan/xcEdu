package com.xuecheng.manage_cms.service;
import com.xuecheng.framework.domain.cms.CmsTemplate;
import com.xuecheng.framework.domain.course.CourseBase;
import com.xuecheng.framework.model.response.CommonCode;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.QueryResult;
import com.xuecheng.manage_cms.dao.CmsTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CmsTemplateService {
    @Autowired
    private CmsTemplateRepository cmsTemplateRepository;

    public QueryResponseResult findList(){
        List<CmsTemplate> all = cmsTemplateRepository.findAll();
        //设置参数内容
        QueryResult<CmsTemplate> queryResult = new QueryResult<CmsTemplate>();
        queryResult.setList(all);
        QueryResponseResult queryResponseResult = new QueryResponseResult(CommonCode.SUCCESS,queryResult);
        return queryResponseResult;
    }
}
