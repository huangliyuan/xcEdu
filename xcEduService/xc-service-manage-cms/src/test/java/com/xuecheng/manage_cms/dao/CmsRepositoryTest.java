package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.hibernate.engine.jdbc.Size;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CmsRepositoryTest {

    @Autowired
    private CmsPageRepository cmsPageRepository;

    /**
     * 查询全部
     */
    @Test
    public void testFindAll(){
        List<CmsPage> all = cmsPageRepository.findAll();
        System.out.println(all.size());
    }

    /**
     * 分页查询
     */
    @Test
    public void testFindPage(){
        int page = 0;//一般从0开始
        int size = 10;
        Pageable pageable = PageRequest.of(page, size);
        Page<CmsPage> all = cmsPageRepository.findAll(pageable);
        System.out.println(all);
        System.out.println("查询到的数据内容数量："+all.getSize());
    }

    /**
     * 修改
     */
    @Test
    public void testUpdate(){
        Optional<CmsPage> optional = cmsPageRepository.findById("5a754adf6abb500ad05688d9");
        if(optional.isPresent()){
            CmsPage cmsPage = optional.get();
            cmsPage.setPageName("index.html");
            cmsPageRepository.save(cmsPage);
        }
    }

    /**
     * 自定义方法
     */
    @Test
    public void testFindByName(){
        CmsPage cmsPage = cmsPageRepository.findByPageName("index.html");
        System.out.println(cmsPage);
    }

    /**
     * 自定义查询条件查询内容
     */
    @Test
    public void testFindAllByExample(){
        //设置分页条件
        int page = 0;//一般从0开始
        int size = 10;
        Pageable pageable = PageRequest.of(page,size);
        //封装条件
        CmsPage cmsPage = new CmsPage();
            //设置站点id精确匹配
        //cmsPage.setSiteId("5a751fab6abb5044e0d19ea1");
            //设置模板id精确匹配
        //cmsPage.setTemplateId("5a962bf8b00ffc514038fafa");
            //设置页面别名模糊查询内容
        cmsPage.setPageAliase("轮播");
        //定义条件匹配器
        ExampleMatcher exampleMatcher = ExampleMatcher.matching();
            //设置模糊查询的条件是包含
        exampleMatcher = exampleMatcher.withMatcher("pageAliase",ExampleMatcher.GenericPropertyMatchers.contains());
        //封装查询条件
        Example<CmsPage> example = Example.of(cmsPage,exampleMatcher);

        Page<CmsPage> all = cmsPageRepository.findAll(example, pageable);
        List<CmsPage> content = all.getContent();
        System.out.println(content);
    }
}
